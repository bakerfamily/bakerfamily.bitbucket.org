$(function() {
       $('.content').css('height', ($(window).height()-240)*(0.7));
       $(window).resize(function(){
               $('.content').css('height', ($(window).height()-240)*(0.7));
       });
       $('.main').css('height', ($(window).height())*(0.921));
       $(window).resize(function(){
               $('.main').css('height', ($(window).height())*(0.921));
       });

       $('#da-slider').cslider({
            bgincrement : 0
        });

       $('#contact-box').collapse();
});



$(document).ready(function() {
	/**
	 * Resizes the content box until a CSS fix can be found
	 */
	$('.content').css('height', ($(window).height()-240)*(0.7));
	$(window).resize(function(){
		$('.content').css('height', ($(window).height()-240)*(0.7));
	});

	var hideContent = function() {
		$('.content').toggle('blind');
	};


	/**
	 * OnClick handler for the top navigation
	 */
	$('.main').on('click', '.content-header li a', function(e) {
		e.preventDefault();
		$content = $('.content');
		var link = $(this).attr('href');
		$('.content').load(link + ' .content div');
	});

	/**
	 * onClick handler for the main navigation
	 */
	$('nav li a').on('click', function(e) {
		e.preventDefault();
		var link = $(this).attr('href');
		hideContent();
		$('.main').load(link + ' .main', function() {
			$('.content').hide().toggle('blind');
		});
		//$('.main .content-header').show();
		$('.content').css('height', ($(window).height()-240)*(0.7));
	});

	$('.main').on('click', '.to-hide', hideContent)
	
});

