'use strict';

angular.module('HashBangURLs', []).config(['$locationProvider', function($location) {
  $location.hashPrefix('!');
}]);

var fitnessApp = angular.module('fitnessApp', ['HashBangURLs', 'ui.bootstrap.accordion'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/philosophy', {
        templateUrl: 'views/philosophy.html',
        controller: 'MainCtrl'
      })
      .when('/classes', {
        templateUrl: 'views/classes.html',
        controller: 'MainCtrl'
      })
      .when('/flippers', {
        templateUrl: 'views/flippers.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
